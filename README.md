**Запустить сервис можно командой** ```docker-compose up runserver```

Получить список всех авторов:
```curl '<host:port>/authors/'```

Получить список всех книг автора:
```curl '<host:port>/authors/<author_id>/'```

Редактировать автора книги:
```curl -F 'author=<author_id>' -X PUT '<host:port>/books/<book_id>/'```

Удаление автора:
```curl -X DELETE '<host:port>/authors/<author_id>/'```

Добавить книгу:
```curl -F 'publish_date=2010-01-01 00:00' -F 'title=' -F 'edition=' -F 'author=<author_id>' -X POST '<host:port>/books/'```
В примере перечисленные поля являются обязательными. Если передать не все, либо передать ИД несуществующего автора, книга не будет сохранена, вернетя ответ со статусом 400
