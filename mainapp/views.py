from django.shortcuts import render
from .models import Book, Author
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_202_ACCEPTED, HTTP_404_NOT_FOUND
from .serializers import BookSerializer, AuthorSerializer


# Create your views here.

class Authors(ViewSet):
    def list(self, request, *args, **kwargs):
        authors = Author.objects.all()
        serializer = AuthorSerializer(authors, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        try:
            author = Author.objects.get(id=kwargs.get('pk'))
        except Author.DoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)
        books = Book.objects.filter(author=author)
        print(books)
        book_serializator = BookSerializer(books, many=True)
        return Response(book_serializator.data)

    def destroy(self, request, *args, **kwargs):
        try:
            author = Author.objects.get(id=kwargs.get('pk'))
        except Author.DoesNotExist:
            return Response(status=HTTP_400_BAD_REQUEST)
        author.delete()
        return Response(status=HTTP_202_ACCEPTED)


class Books(ViewSet):
    def create(self, request, *args, **kwargs):
        book = BookSerializer(data=request.data)
        if book.is_valid(raise_exception=True):
            book.save()
            return Response(status=HTTP_202_ACCEPTED)
        else:
            return Response(status=HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        try:
            book = Book.objects.get(id=kwargs.get('pk'))
        except Book.DoesNotExist:
            return Response(status=HTTP_400_BAD_REQUEST)
        if 'author' not in request.data:
            return Response(status=HTTP_400_BAD_REQUEST)
        book_serializer = BookSerializer(instance=book, data={'author': request.data.get('author')}, partial=True)
        if book_serializer.is_valid(raise_exception=True):
            book_serializer.save()
            return Response(status=HTTP_202_ACCEPTED)
        else:
            return Response(status=HTTP_400_BAD_REQUEST)
