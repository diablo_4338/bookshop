from django.db import models


# Create your models here.

class Author(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    country = models.CharField(max_length=50)
    birth_year = models.IntegerField()

    def __str__(self):
        return str(self.first_name) + ' ' + str(self.last_name)


class Book(models.Model):
    publish_date = models.DateTimeField()
    title = models.CharField(max_length=100)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    edition = models.CharField(max_length=20)

    def __str__(self):
        return str(self.title)
